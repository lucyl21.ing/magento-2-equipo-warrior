# magento 2 
## _Equipo Warriors_

Proyecto de tienda para el curso de agentes 3D Adv
Para ejecutar este demo es importante tener los siguientes requisitos:
-	Ubuntu server 20.04.2 LTS
-	Apache 2.4.41 (Ubuntu)
-	Php 7.4.20
-	Mysql 10.4.20-MariaDB-1

## Caracteristicas
_Tienda de equipos electronicos_
- Inventario con stock y sin stock
- Envios: recoger en tienda y envio a domicilio
- Compra como invitado
- lista de productos mas vendidos

## Tecnologia
Magento 2.4
-	Apache 2.4.41 (Ubuntu)
-	Php 7.4.20
-	Mysql 10.4.20-MariaDB-1


## Instalacion

copiar el proyecto
```sh
cd folder destino
git clone https://gitlab.com/azaelkaos/magento-2-equipo-warrior.git
```

instalar base de datos

```sh
sudo mysql -u tusuario -p -e "CREATE DATABASE magento"
sudo mysql -u tusuario -p magento < magento_dump.sql
```
